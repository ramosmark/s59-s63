import { useState, useEffect } from 'react'
import { Form, Button } from 'react-bootstrap'
import Swal from 'sweetalert2'

export default function EditProduct() {
	const id = window.location.pathname.substring(13, 40)
	const [product, setProduct] = useState()
	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')

	useEffect(() => {
		fetch(`http://localhost:4000/products/${id}`)
			.then((res) => res.json())
			.then((data) => {
				console.log(data)
				setProduct(data)
				setName(data.name)
				setDescription(data.description)
				setPrice(data.price)
			})
	}, [])

	const edit = (e) => {
		e.preventDefault()

		console.log('dsda')

		fetch(`http://localhost:4000/products/${id}`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-type': 'application/json',
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: Number(price),
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				console.log(data)
				if (data.success) {
					Swal.fire({
						title: 'Product has been edited',
						icon: 'success',
						text: data.message,
					})
				} else {
					Swal.fire({
						title: 'Editing product failed',
						icon: 'error',
						text: data.message,
					})
				}
			})
	}

	return (
		<>
			<h1 className="text-center my-2">Edit Product</h1>
			<Form onSubmit={(e) => edit(e)}>
				<Form.Group className="mb-3" controlId="productName">
					<Form.Label>Name</Form.Label>
					<Form.Control
						type="text"
						value={name}
						onChange={(e) => setName(e.target.value)}
						placeholder="Enter Product Name"
					/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="productDescription">
					<Form.Label>Description</Form.Label>
					<Form.Control
						value={description}
						onChange={(e) => setDescription(e.target.value)}
						type="text"
						placeholder="Enter Product Price"
					/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="productPrice">
					<Form.Label>Price</Form.Label>
					<Form.Control
						type="text"
						value={price}
						onChange={(e) => setPrice(e.target.value)}
						placeholder="Enter Product Price"
					/>
				</Form.Group>

				{/* <Form.Group className="mb-3" controlId="quantity">
					<Form.Label>Quantity</Form.Label>
					<Form.Control type="text" placeholder="Input Quantity" />
				</Form.Group> */}

				<Button variant="success" type="submit">
					Edit
				</Button>
			</Form>
		</>
	)
}
