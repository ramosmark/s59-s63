import { useState, useEffect } from 'react'
import { Container, Table } from 'react-bootstrap'
import TableRow from '../components/TableRow'

export default function AdminAllProduct() {
	const [products, setProducts] = useState([])

	useEffect(() => {
		fetch('http://localhost:4000/products/')
			.then((res) => res.json())
			.then((data) => {
				setProducts(data)
			})
	}, [])

	return (
		<Container>
			<h1 className="text-center">Admin Dashboard</h1>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>Product Name</th>
						<th>Product Description</th>
						<th>Product Price</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{products.map((product) => {
						return <TableRow data={product} key={product._id} />
					})}
				</tbody>
			</Table>
		</Container>
	)
}
