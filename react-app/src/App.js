import { useState, useEffect } from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import { Routes, Route } from 'react-router-dom'
import { Container } from 'react-bootstrap'

import Navbar from './components/Navbar'
import { UserProvider } from './UserContext'

import Login from './pages/Login'
import Logout from './pages/Logout'
import Register from './pages/Register'
import AdminAllProducts from './pages/AdminAllProduct'
import CreateProduct from './pages/CreateProduct'
import EditProduct from './pages/EditProduct'
import AllProducts from './pages/AllProducts'
import Product from './pages/Product'
import ArchivedProducts from './pages/ArchivedProducts'
import './App.css'

function App() {
	const [user, setUser] = useState({ id: null, isAdmin: null })

	const unsetUser = () => {
		localStorage.clear()
	}

	useEffect(() => {
		fetch(`http://localhost:4000/users/details`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				if (typeof data._id !== undefined) {
					setUser({
						id: data._id,
						isAdmin: data.isAdmin,
					})
				} else {
					setUser({
						id: null,
						isAdmin: null,
					})
				}
			})
	}, [])

	return (
		<UserProvider value={{ user, setUser, unsetUser }}>
			<Router>
				<Navbar />
				<Container>
					<Routes>
						<Route path="/" element={<AllProducts />} />
						<Route path="/login" element={<Login />} />
						<Route path="/logout" element={<Logout />} />
						<Route path="/register" element={<Register />} />
						<Route path="/adminProducts" element={<AdminAllProducts />} />
						<Route path="/archivedProducts" element={<ArchivedProducts />} />
						<Route path="/createProduct" element={<CreateProduct />} />
						<Route path="/editProduct/:productId" element={<EditProduct />} />
						<Route path="/products/productId" element={<Product />} />
						<Route path="/products" element={<AllProducts />} />
						<Route path="/products/:productId" element={<Product />} />
					</Routes>
				</Container>
			</Router>
		</UserProvider>
	)
}

export default App
