const bcrypt = require('bcrypt')
const User = require('../models/User')
const auth = require('../auth')
const Product = require('../models/Product')

module.exports.registerUser = (reqBody) => {
	const newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
	})

	return User.find({ email: reqBody.email }).then((result) => {
		if (result.length > 0) {
			return {
				success: false,
				message: 'Email has been used. Try another email address',
			}
		} else {
			return newUser.save().then((user, error) => {
				if (error) {
					return { success: false, message: 'Invalid registration' }
				} else {
					return { success: true, message: 'You have been registered!' }
				}
			})
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({ email: reqBody.email }).then((result) => {
		if (result === null) {
			return { sucess: false, message: 'Please check if your email is correct' }
		} else {
			const isPasswordCorrect = bcrypt.compareSync(
				reqBody.password,
				result.password
			)

			if (isPasswordCorrect) {
				return { success: true, access: auth.createAccessToken(result) }
			} else {
				return { success: false, message: 'Password is Incorrect' }
			}
		}
	})
}

module.exports.createOrder = async (data) => {
	// console.log(data)
	if (data.isAdmin) {
		return { success: false, message: `Admin can't create order` }
	} else {
		const product = await Product.findById(data.productId).then(
			(product) => product
		)

		const isUserUpdated = await User.findById(data.userId).then((user) => {
			user.orders.push({
				products: {
					productName: product.name,
					quantity: data.quantity,
				},
				totalAmount: product.price * data.quantity,
			})

			return user.save().then((user, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			})
		})

		const isProductUpdated = await Product.findById(data.productId).then(
			(product) => {
				product.orders.push({ orderId: data.userId })

				return product.save().then((product, error) => {
					if (error) {
						return false
					} else {
						return true
					}
				})
			}
		)

		if (isUserUpdated && isProductUpdated) {
			return { success: true, message: 'Ordered successfully' }
		} else {
			return {
				success: false,
				message: `Can't process order. Error encountered`,
			}
		}
	}
}

module.exports.getDetails = (userData) => {
	return User.findById(userData.id).then((result) => {
		if (result) {
			return { success: true, user: result }
		} else {
			return false
		}
	})
}
