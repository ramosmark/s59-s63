const Product = require('../models/Product')
const auth = require('../auth')
const User = require('../models/User')

module.exports.addProduct = (req) => {
	const userData = auth.decode(req.headers.authorization)

	return User.findById(userData.id).then((result) => {
		if (result.isAdmin) {
			const newProduct = Product({
				name: req.body.name,
				description: req.body.description,
				price: req.body.price,
			})

			return newProduct.save().then((product, error) => {
				if (error) {
					return { success: false, message: 'Encountered an error. Try again' }
				} else {
					return { success: true, message: 'Product has been created' }
				}
			})
		} else {
			return { success: false, message: 'Only admin can create a new product' }
		}
	})
}

module.exports.getAllActiveProducts = () => {
	return Product.find({ isActive: true }).then((result) => result)
}

module.exports.getInactiveProducts = () => {
	return Product.find({ isActive: false }).then((result) => result)
}

module.exports.getProduct = (reqParam) => {
	return Product.findById(reqParam.productId).then((result) => result)
}

module.exports.updateProduct = (req) => {
	const userData = auth.decode(req.headers.authorization)
	const productId = req.params.productId
	const reqBody = req.body

	return User.findById(userData.id).then((result) => {
		if (userData.isAdmin) {
			const updateProduct = {
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price,
			}

			return Product.findByIdAndUpdate(productId, updateProduct).then(
				(updatedProduct, error) => {
					if (error) {
						return {
							success: false,
							message: 'Editing failed. Please Try again',
						}
					} else {
						return { success: true, message: 'Product has been edited' }
					}
				}
			)
		} else {
			return false
		}
	})
}

module.exports.archiveProduct = (req) => {
	const userData = auth.decode(req.headers.authorization)
	const productId = req.params.productId

	return User.findById(userData.id).then((result) => {
		if (userData.isAdmin) {
			return Product.findByIdAndUpdate(productId, { isActive: false }).then(
				(updatedProduct, error) => {
					if (error) {
						return { sucess: false, message: 'Failed to archive product.' }
					} else {
						return { success: true, message: 'Product has been archived' }
					}
				}
			)
		} else {
			return { success: false, message: 'You are not an admin' }
		}
	})
}

module.exports.unarchiveProduct = (req) => {
	const userData = auth.decode(req.headers.authorization)
	const productId = req.params.productId

	return User.findById(userData.id).then((result) => {
		if (userData.isAdmin) {
			return Product.findByIdAndUpdate(productId, { isActive: true }).then(
				(updatedProduct, error) => {
					if (error) {
						return {
							sucess: false,
							message: 'Failed to activate product.',
						}
					} else {
						return {
							success: true,
							message: 'Product has been activated',
							updatedProduct,
						}
					}
				}
			)
		} else {
			return { success: false, message: 'You are not an admin' }
		}
	})
}
