const express = require('express')
const router = express.Router()
const userController = require('../controllers/userController')
const auth = require('../auth')

router.post('/register', (req, res) => {
	userController.registerUser(req.body).then((result) => {
		res.send(result)
	})
})

router.post('/login', (req, res) => {
	userController.loginUser(req.body).then((result) => {
		res.send(result)
	})
})

router.post('/orders', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	const data = {
		userId: req.body.userId,
		productId: req.body.productId,
		quantity: req.body.quantity,
		isAdmin: userData.isAdmin,
	}

	userController.createOrder(data).then((result) => {
		res.send(result)
	})
})

router.get('/details', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	userController
		.getDetails({ id: userData.id })
		.then((result) => res.send(result))
})

module.exports = router
